﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAL.Repositories.Generic.Repository
{
   public static class DGeneric 
    {
        public static string CnnStrVariableName = "CnnStr";
        private static string _ConnectionString = string.Empty;
        public static void SetConnectionString(string cnnStr)
        {
            if (cnnStr.Trim().Length == 0)
            {
                _ConnectionString = ConfigurationManager.ConnectionStrings[CnnStrVariableName].ConnectionString;
            }
            else
            {
                _ConnectionString = cnnStr;
            }
        }
        private static string ConnectionString
        {
            get
            {
                if (_ConnectionString.Trim().Length == 0)
                {
                    SetConnectionString(string.Empty);
                }
                return DGeneric._ConnectionString;
            }
            set
            {

                DGeneric._ConnectionString = value;
            }
        }
        public static bool ExecQuery(string query)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = query;
            //sqlCommand.CommandType = CommandType.StoredProcedure;
            string str = DGeneric.ConnectionString;
            SqlConnection cnn = new SqlConnection(str);
            cnn.Open();

            sqlCommand.Connection = cnn;

            //try
            //{



            sqlCommand.ExecuteNonQuery();
            cnn.Close();
            sqlCommand.Dispose();
            return true;
            //}
            //catch (Exception ex)
            //{
            //    cnn.Close();
            //    sqlCommand.Dispose();
            //    throw new Exception("cls_ScrapedData::Insert::Error occured.", ex.InnerException);
            //}

        }
        public static object GetValue(string query)
        {
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = query;
            //sqlCommand.CommandType = CommandType.StoredProcedure;
            string str = DGeneric.ConnectionString;
            SqlConnection cnn = new SqlConnection(str);
            cnn.Open();

            sqlCommand.Connection = cnn;

            //try
            //{
            object obj = sqlCommand.ExecuteScalar();
            cnn.Close();
            sqlCommand.Dispose();
            return obj;
            //}
            //catch (Exception ex)
            //{
            //    cnn.Close();
            //    sqlCommand.Dispose();
            //    throw new Exception("cls_ScrapedData::Insert::Error occured.", ex.InnerException);
            //}

        }
        public static DataSet GetData(string query)
        {
            SqlConnection cnn = new SqlConnection(DGeneric.ConnectionString);
            SqlCommand sqlCommand = new SqlCommand();
            sqlCommand.CommandText = query;
            //sqlCommand.CommandType = CommandType.StoredProcedure;
            //cnn.Open();
            sqlCommand.Connection = cnn;
            //try
            //{
            SqlDataAdapter objDa = new SqlDataAdapter(sqlCommand);
            DataSet ds = new DataSet();
            objDa.Fill(ds);
            //cnn.Close();
            sqlCommand.Dispose();
            return ds;
            //}
            //catch (Exception ex)
            //{
            //    cnn.Close();
            //    sqlCommand.Dispose();
            //    throw new Exception("cls_ScrapedData::Insert::Error occured.", ex.InnerException);
            //}

        }

    }
}
