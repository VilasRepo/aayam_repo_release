﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Master;
using ViewModel.Student;

namespace DAL.Repositories.Student.Interface
{
    public interface IDStudent
    {
        List<StudentViewModel> StudentSelect_Joined();
        List<StudentViewModel> StudentSelect_NotJoined();
        bool SetStudent_Joined(int Id);
        bool SetStudent_NotJoined(int Id);
       
        //---------------------------Enquiry Methods-----------------------------//
        bool EnquiryInsert(StudentViewModel stu);
        List<StudentViewModel> EnquirySelect();

        //----------------------------General Methods----------------------------//
        List<Country> GetCountry();
        List<State> GetState(int countryId);
        List<City> GetCity(int stateId);
        List<Course> GetCourse();
    
    }
}
