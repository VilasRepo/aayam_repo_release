﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DAL.Repositories.Student.Interface;
using ViewModel.Student;
using System.Data.SqlClient;
using System.Data;
using ViewModel.Master;

namespace DAL.Repositories.Student.Repository
{
    public class DStudent : IDStudent
    {
        public List<StudentViewModel> StudentSelect_Joined()
        {
            try
            {
                List<StudentViewModel> StudentList = new List<StudentViewModel>();
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand cmd = new SqlCommand("sp_StudentSelectJoined", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
              
               while(reader.Read())
                {

                    StudentList.Add(

                        new StudentViewModel
                        {

                            StudentID=Convert.ToInt32(reader["StudentID"]),
                            EnrollmentNo = reader["EnrollmentNo"].ToString(),
                            Name = reader["FullName"].ToString(),
                            Course = reader["Course"].ToString(),
                            MobileNo = reader["MobileNumber"].ToString(),
                        }
                        );
                    

                }
               con.Close();
                return StudentList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<StudentViewModel> StudentSelect_NotJoined()
        {
            try
            {
                List<StudentViewModel> StudentList = new List<StudentViewModel>();
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand cmd = new SqlCommand("sp_StudentSelectNotJoined", con);
                cmd.CommandType = CommandType.StoredProcedure;
                con.Open();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    StudentList.Add(
                        new StudentViewModel
                        {
                            StudentID = Convert.ToInt32(reader["StudentID"]),
                            Name = reader["FullName"].ToString(),
                            Course = reader["Course"].ToString(),
                            MobileNo = reader["MobileNumber"].ToString(),
                        }
                        );
                }
                con.Close();
                return StudentList;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool SetStudent_Joined(int Id)
        {
            try
            {
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand cmd = new SqlCommand("sp_SetStudentIsJoined", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StudentID", Id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public bool SetStudent_NotJoined(int Id)
        {
            try
            {
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand cmd = new SqlCommand("sp_SetStudentIsNotJoined", con);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@StudentID", Id);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return true;

            }
            catch (Exception)
            {

                throw;
            }
        }
        //---------------------------Enquiry Methods-----------------------------//
        public bool EnquiryInsert(StudentViewModel stu)
        {
            try
            {
                using (SqlConnection con = Factory.getSqlInstance())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_StudentInsert", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@StudentCode", 1);
                    cmd.Parameters.AddWithValue("@EnquiryStatusID", null);
                    cmd.Parameters.AddWithValue("@EnrollmentNo", string.Empty);
                    cmd.Parameters.AddWithValue("@EnrollmentDate", DateTime.Now);
                    cmd.Parameters.AddWithValue("@AdharNo", !string.IsNullOrEmpty(stu.Aadhar) ? stu.Aadhar : null);
                    cmd.Parameters.AddWithValue("@FirstName", stu.FirstName);
                    cmd.Parameters.AddWithValue("@MiddleName", !string.IsNullOrEmpty(stu.MiddleName) ? stu.MiddleName : null);
                    cmd.Parameters.AddWithValue("@LastName", stu.LastName);
                    cmd.Parameters.AddWithValue("@Gender", stu.Gender);
                    cmd.Parameters.AddWithValue("@DOB", null);
                    cmd.Parameters.AddWithValue("@MobileNumber", stu.MobileNo);
                    cmd.Parameters.AddWithValue("@PhoneNumber", null);
                    cmd.Parameters.AddWithValue("@Email", !string.IsNullOrEmpty(stu.Email) ? stu.Email : string.Empty);
                    cmd.Parameters.AddWithValue("@Address", !string.IsNullOrEmpty(stu.Address) ? stu.Address : string.Empty);
                    cmd.Parameters.AddWithValue("@Landmark", !string.IsNullOrEmpty(stu.Landmark) ? stu.Landmark : string.Empty);
                    cmd.Parameters.AddWithValue("@CityID_P", stu.CityId);
                    cmd.Parameters.AddWithValue("@Pin", stu.Pincode);
                    cmd.Parameters.AddWithValue("@Cast", null);
                    cmd.Parameters.AddWithValue("@PhotoUrl1", string.Empty);
                    cmd.Parameters.AddWithValue("@StreamID", 1);
                    cmd.Parameters.AddWithValue("@CourseID", stu.CourseId);
                    cmd.Parameters.AddWithValue("@BatchID", 1);
                    cmd.Parameters.AddWithValue("@SessionID", null);
                    cmd.Parameters.AddWithValue("@SchoolCollege", null);
                    cmd.Parameters.AddWithValue("@FatherName", !string.IsNullOrEmpty(stu.FatherName) ? stu.FatherName : string.Empty);
                    cmd.Parameters.AddWithValue("@FatherOccupation", null);
                    cmd.Parameters.AddWithValue("@FatherMobile", !string.IsNullOrEmpty(stu.FatherMobile) ? stu.FatherMobile : string.Empty);
                    cmd.Parameters.AddWithValue("@FatherEmail", null);
                    cmd.Parameters.AddWithValue("@CreatedByUserID", 123);
                    cmd.Parameters.AddWithValue("@StudentId", SqlDbType.Int).Direction = ParameterDirection.Output;
                    cmd.ExecuteNonQuery();
                    int id = (int)cmd.Parameters["@StudentId"].Value;

                    SqlCommand cmm = new SqlCommand("sp_EnquiryInsert", con);
                    cmm.CommandType = CommandType.StoredProcedure;
                    cmm.Parameters.AddWithValue("@EnquiryDate", DateTime.Parse(stu.EnquiryDate));
                    cmm.Parameters.AddWithValue("@StudentID", id);
                    cmm.Parameters.AddWithValue("@SessionID", 1);
                    cmm.Parameters.AddWithValue("@StreamID", 1);
                    cmm.Parameters.AddWithValue("@CourseID", 1);
                    cmm.Parameters.AddWithValue("@Source", string.IsNullOrEmpty(stu.Source) ? stu.Source : string.Empty);
                    cmm.Parameters.AddWithValue("@AssignedTo", 1);
                    cmm.Parameters.AddWithValue("@FollowUpDate", DateTime.Parse(stu.FollowUpDate));
                    cmm.Parameters.AddWithValue("@Enquiry", string.IsNullOrEmpty(stu.EnquiryDescription) ? stu.EnquiryDescription : string.Empty);
                    cmm.Parameters.AddWithValue("@CreatedByUserID", 123);
                    cmm.ExecuteNonQuery();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw;
            }
        }
        public List<StudentViewModel> EnquirySelect()
        {
            List<StudentViewModel> studentList = new List<StudentViewModel>();
            try
            {
                using (SqlConnection con = Factory.getSqlInstance())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_EnquirySelect", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    con.Close();
                    foreach (DataRow dr in dt.Rows)
                    {
                        studentList.Add(new StudentViewModel
                        {
                            StudentID = Convert.ToInt32(dr["StudentID"]),
                            EnrollmentNo = dr["EnrollmentNo"].ToString(),
                            FirstName = dr["FullName"].ToString(),
                            Course = dr["Course"].ToString(),
                            MobileNo = dr["MobileNumber"].ToString(),
                        });
                    }
                }
                return studentList;
            }
            catch (Exception ex)
            {
                // return ex.Message;
                throw;
            }
        }

        //----------------------------General Methods----------------------------//
        public List<Country> GetCountry()
        {
            List<Country> countryList = new List<Country>();
            try
            {
                using (SqlConnection con = Factory.getSqlInstance())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_GetCountryStateCity", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    con.Close();
                    foreach (DataRow dr in dt.Rows)
                    {
                        countryList.Add(new Country
                        {
                            CountryId = Convert.ToInt32(dr["id"]),
                            CountryName = dr["name"].ToString(),

                        });
                    }
                }
                return countryList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<State> GetState(int countryId)
        {
            List<State> stateList = new List<State>();
            try
            {
                using (SqlConnection con = Factory.getSqlInstance())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_GetCountryStateCity", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@CountryId", countryId);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    con.Close();
                    foreach (DataRow dr in dt.Rows)
                    {
                        stateList.Add(new State
                        {
                            StateId = Convert.ToInt32(dr["id"]),
                            StateName = dr["name"].ToString(),

                        });
                    }
                }
                return stateList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<City> GetCity(int stateId)
        {
            List<City> cityList = new List<City>();
            try
            {
                using (SqlConnection con = Factory.getSqlInstance())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("sp_GetCountryStateCity", con);
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@StateId", stateId);
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    con.Close();
                    foreach (DataRow dr in dt.Rows)
                    {
                        cityList.Add(new City
                        {
                            CityId = Convert.ToInt32(dr["id"]),
                            CityName = dr["name"].ToString(),

                        });
                    }
                }
                return cityList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<Course> GetCourse()
        {
            List<Course> countryList = new List<Course>();
            try
            {
                using (SqlConnection con = Factory.getSqlInstance())
                {
                    con.Open();
                    SqlCommand cmd = new SqlCommand("select * from course", con);
                    cmd.CommandType = CommandType.Text;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    con.Close();
                    foreach (DataRow dr in dt.Rows)
                    {
                        countryList.Add(new Course
                        {
                            CourseID = Convert.ToInt32(dr["CourseID"]),
                            CourseName = dr["Course"].ToString(),

                        });
                    }
                }
                return countryList;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
