﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Master;

namespace DAL.Repositories.Admin.Interface
{
    public interface IDMaster
    {
        //---------------------------Stream Methods-----------------------------//
         bool StreamInsert(Streams objStream);
         List<Streams> StreamSelect();
         bool StreamUpdate(int Id,Streams objStream);
         bool StreamDelete(int Id);

         //---------------------------Course Methods-----------------------------//
         bool CourseInsert(Course objCourse);
         List<CourseViewModel> CourseSelect();
         bool CourseUpdate(int Id, Course objCourse);
         bool CourseDelete(int Id);
         
        
         
    
    }
}
