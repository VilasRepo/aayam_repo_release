﻿using DAL.Repositories.Admin.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Threading.Tasks;
using ViewModel.Master;
using System.Configuration;
using Generic.Data;
using Generic.Utilities;
using DAL.Repositories.Generic.Repository;


namespace DAL.Repositories.Admin.Repository
{
    public class DMaster : IDMaster
    {
        #region Fields
        private string connectionStringName;
        #endregion

        #region Constructor
        public DMaster()
        {
            ValidationUtility.ValidateArgument("connectionStringName", DGeneric.CnnStrVariableName);

            //this.connectionStringName = DGeneric.CnnStrVariableName;
            this.connectionStringName = DGeneric.CnnStrVariableName;
        }
        #endregion

        #region Stream Methods
        public bool StreamInsert(Streams objStream)
        {
            try
            {
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand com = new SqlCommand("sp_StreamInsert", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Stream", objStream.StreamName);
                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();
                if (i >= 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
        public List<Streams> StreamSelect()
        {
            try
            {
                using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "sp_StreamSelect"))
                {
                    List<Streams> StreamsList = new List<Streams>();
                    while (dataReader.Read())
                    {
                        //StreamsList.Add(
                        //   new Streams
                        //   {
                        //       StreamID = Convert.ToInt32(dataReader["StreamID"]),
                        //       StreamName = Convert.ToString(dataReader["Stream"]),
                        //   }
                        //   );
                        Streams objStreams = MapDataReader_Streams(dataReader);
                        StreamsList.Add(objStreams);
                    }
                    return StreamsList;
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
        private Streams MapDataReader_Streams(SqlDataReader dataReader)
        {
            Streams objStreams = new Streams();
            objStreams.StreamID = Convert.ToInt32(dataReader["StreamID"]);
            objStreams.StreamName = Convert.ToString(dataReader["Stream"]);
            return objStreams;
        }
        public bool StreamUpdate(int Id, Streams objStream)
        {

            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_StreamUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@StreamId", Id);
            com.Parameters.AddWithValue("@StreamName", objStream.StreamName);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool StreamDelete(int Id)
        {

            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_StreamDelete", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@StreamID", Id);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        #endregion

        #region Course Methods
        public bool CourseInsert(Course objCourse)
        {
            try
            {
                SqlConnection con = Factory.getSqlInstance();
                SqlCommand com = new SqlCommand("sp_CourseInsert", con);
                com.CommandType = CommandType.StoredProcedure;
                com.Parameters.AddWithValue("@Course", objCourse.CourseName);
                com.Parameters.AddWithValue("@StreamID", objCourse.StreamID);
                //com.Parameters.AddWithValue("@StreamID", objCourse.StreamID);
                con.Open();
                int i = com.ExecuteNonQuery();
                con.Close();
                if (i != 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {

                throw new Exception();
            }

            //SqlParameter[] parameters = new SqlParameter[]
            //{
            //    new SqlParameter("@Course", objCourse.CourseName),
            //    new SqlParameter("@StreamID", objCourse.StreamID)
            //};
            //  objCourse.CourseID = Convert.ToInt32(ExecuteScalar(Factory.getSqlInstance().ToString(), CommandType.StoredProcedure, "sp_CourseInsert", parameters));

        }
        public List<CourseViewModel> CourseSelect()
        {
            try
            {
                using (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, "sp_CourseSelect"))
                {
                    List<CourseViewModel> CourseList = new List<CourseViewModel>();
                    while (dataReader.Read())
                    {
                        CourseList.Add(

                           new CourseViewModel
                           {
                               CourseID = Convert.ToInt32(dataReader["CourseID"]),
                               StreamID = Convert.ToInt32(dataReader["StreamID"]),
                               CourseName = Convert.ToString(dataReader["Course"]),
                               Stream = Convert.ToString(dataReader["Stream"]),
                           }
                           );
                        //CourseViewModel objCourse = MapDataReader_Course(dataReader);
                        //CourseList.Add(objCourse);
                    }
                    return CourseList;
                }
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }
        private CourseViewModel MapDataReader_Course(SqlDataReader dataReader)
        {
            CourseViewModel objCourse = new CourseViewModel();
            objCourse.StreamID = Convert.ToInt32(dataReader["StreamID"]);
            objCourse.CourseID = Convert.ToInt32(dataReader["CourseID"]);
            objCourse.CourseName = Convert.ToString(dataReader["Course"]);
            objCourse.Stream = Convert.ToString(dataReader["Stream"]);

            return objCourse;
        }
        public bool CourseUpdate(int Id, Course objCourse)
        {
            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_CourseUpdate", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CourseID", Id);
            com.Parameters.AddWithValue("@Course", objCourse.CourseName);
            com.Parameters.AddWithValue("@StreamID", objCourse.StreamID);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool CourseDelete(int Id)
        {
            SqlConnection con = Factory.getSqlInstance();
            SqlCommand com = new SqlCommand("sp_CourseDelete", con);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.AddWithValue("@CourseID", Id);
            con.Open();
            int i = com.ExecuteNonQuery();
            con.Close();
            if (i >= 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

    }
}
