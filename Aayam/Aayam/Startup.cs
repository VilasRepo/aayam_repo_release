﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Aayam.Startup))]
namespace Aayam
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
