﻿using DAL.Repositories.Admin.Interface;
using DAL.Repositories.Admin.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ViewModel.Master;

namespace Aayam.Controllers
{
    public class MasterController : Controller
    {
        private readonly IDMaster _idMaster;
        public MasterController()
        {
            _idMaster = new DMaster();
        }
        
        //---------------------------Stream Methods-----------------------------//
        public ActionResult StreamSelect()
        {
            ModelState.Clear();
            return View(_idMaster.StreamSelect());
        }
        public ActionResult StreamInsert()
        {
            return View();
        }
        [HttpPost]
        public ActionResult StreamInsert(Streams objStream)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_idMaster.StreamInsert(objStream))
                    {
                        ViewBag.Message =true;
                    }
                }

                return View();
            }
            catch
            {
                return View();
            }
        }
        public ActionResult StreamUpdate(int Id)
        {
          
            return View(_idMaster.StreamSelect().Find(str => str.StreamID == Id));

        }
        [HttpPost]
        public ActionResult StreamUpdate(int Id, Streams objStream)
        {
            try
            {
                if ( _idMaster.StreamUpdate(Id,objStream))
                {
                    ViewBag.message = true;  
                }
                return RedirectToAction("StreamSelect");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult StreamDelete(int Id)
        {
            try
            {
              
                if (_idMaster.StreamDelete(Id))
                {
                    ViewBag.message = true;

                }
                return RedirectToAction("StreamSelect");

            }
            catch
            {
                return View();
            }
        }

        //---------------------------Course Methods-----------------------------//
        public ActionResult CourseSelect()
        {
            ModelState.Clear();
            return View(_idMaster.CourseSelect());
        }
        public ActionResult CourseInsert()
        {
            ViewBag.Streams = _idMaster.StreamSelect();
            return View();
        }
        [HttpPost]
        public ActionResult CourseInsert(Course objCourse)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (_idMaster.CourseInsert(objCourse))
                    {
                        ViewBag.Message =true;
                    }
                }
                ViewBag.Streams = _idMaster.StreamSelect();
                ModelState.Clear();
                return View();
               
            }
            catch
            {
                return View();
            }
        }
        public ActionResult CourseUpdate(int Id)
        {
            ViewBag.Streams = _idMaster.StreamSelect();
            return View(_idMaster.CourseSelect().Find(str => str.CourseID == Id));
        }
        [HttpPost]
        public ActionResult CourseUpdate(int Id, Course objCourse)
        {
            try
            {
                if (_idMaster.CourseUpdate(Id, objCourse))
                {
                    ViewBag.message = true;
                }
                return RedirectToAction("CourseSelect");
            }
            catch
            {
                return View();
            }
        }
        public ActionResult CourseDelete(int Id)
        {
            try
            {
                if (_idMaster.CourseDelete(Id))
                {
                    ViewBag.message = true;
                }
                return RedirectToAction("CourseSelect");
            }
            catch
            {
                return View();
            }
        }  
  
    }
}
