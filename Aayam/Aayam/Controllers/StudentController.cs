﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DAL.Repositories.Student.Interface;
using DAL.Repositories.Student.Repository;
using ViewModel.Student;

namespace Aayam.Controllers
{
    public class StudentController : Controller
    {
        private readonly IDStudent _idstudent;
        
        public StudentController()
        {
            _idstudent = new DStudent();
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult profile()
        {
            return View();
        }

        //---------------------------Student Methods-----------------------------//
        public ActionResult StudentSelect()
        {
            return View(_idstudent.StudentSelect_Joined());
        }
        public ActionResult StudentSelectNotJoined()
        {
            return View(_idstudent.StudentSelect_NotJoined());
        }
        public ActionResult SetStudentJoined(int id)
        {
            if (_idstudent.SetStudent_Joined(id))
                return RedirectToAction("StudentSelect", "Student");

            return RedirectToAction("", "");
        }
        public ActionResult SetStudentNotJoined(int id)
        {
            if (_idstudent.SetStudent_NotJoined(id))
                return RedirectToAction("StudentSelectNotJoined", "Student");

            return RedirectToAction("", "");
        }

        //---------------------------Enquiry Methods-----------------------------//
        public ActionResult EnquirySelect()
        {
            return View(_idstudent.EnquirySelect());
        }
        public ActionResult EnquiryInsert()
        {
            ViewBag.Country = _idstudent.GetCountry();
            ViewBag.Course = _idstudent.GetCourse();
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public RedirectToRouteResult EnquiryInsert(StudentViewModel objStudent)
        {
            if (ModelState.IsValid)
            {
                _idstudent.EnquiryInsert(objStudent);
            }
            return RedirectToAction("EnquirySelect", "Student");
        }
       
        //----------------------------General Methods----------------------------//
        public JsonResult GetState(int countryId)
        {
            return Json(_idstudent.GetState(countryId), JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCity(int stateId)
        {
            return Json(_idstudent.GetCity(stateId), JsonRequestBehavior.AllowGet);
        }
      
    }
}