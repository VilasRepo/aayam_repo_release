﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ViewModel.Enquiry
{
    public class EnquiryViewModel
    {
        public string Name { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string EnquiryDate { get; set; }
        public int AcademicDate { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int CourseId { get; set; }
        public string Course { get; set; }

        public string Source { get; set; }
        public string AssignedTo { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string FollowUpDate { get; set; }
        public string EnquiryDescription { get; set; }
    }
}
