﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ViewModel.Enquiry;

namespace ViewModel.Student
{
    public class StudentViewModel:EnquiryViewModel
    {
        public int StudentID { get; set; }
        public string EnrollmentNo { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string Gender { get; set; }
        public string DOB { get; set; }
        [Required(ErrorMessage = "This field is required")]
        public string MobileNo { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public string FatherName { get; set; }
        public string FatherMobile { get; set; }
        public string Aadhar { get; set; }
        public string Address { get; set; }
        public string Landmark { get; set; }
        [Display(Name = "Country")]
        public Nullable<int> CountryId { get; set; }
        public int State { get; set; }
        public int CityId { get; set; }
        public int Pincode { get; set; }
        public string Cast { get; set; }
        public string School { get; set; }
       
        
    }
}
